# Base Image for CI Tasks

This image has been created to simplify creation of CI tasks relying on [Docker](https://www.docker.com/) e.g. for generated and deploying application images. For additional use cases [docker-compose](https://docs.docker.com/compose/) has been included.

## Usage

```
docker pull registry.gitlab.com/cepharum-foss/ci-base
``` 

## Scheduled Updates

Once a week the image is rebuilt to keep using latest version of docker and docker-compose.

## GitLab CI Example

```yaml
build:stage:
  image: registry.gitlab.com/cepharum-foss/ci-base
  services:
    - docker:20-dind
  stage: build
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t ${CI_REGISTRY_IMAGE}:dev .
    - docker push ${CI_REGISTRY_IMAGE}:dev
  only:
    - develop

build:production:
  image: registry.gitlab.com/cepharum-foss/ci-base
  services:
    - docker:20-dind
  stage: build
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t ${CI_REGISTRY_IMAGE} .
    - docker push ${CI_REGISTRY_IMAGE}
  only:
    - master
```
