FROM docker:latest

RUN	apk add --no-cache py3-pip python3-dev libffi-dev openssl-dev gcc g++ libc-dev make curl bash rust cargo nodejs-lts npm git jq && \
    docker buildx install && \
	npm i -g yarn && \
	docker -v && \
	docker-compose version && \
	docker compose version && \
	docker context ls && \
	node -v && \
	npm -v

ENV	DOCKER_HOST	"$DOCKER_HOST"
ENV DOCKER_TLS_CERTDIR "/certs"
